const generator = require('../services/generator')

exports.getReview = async (req, res) => {

    let review = await generator.getReview()

    console.log(`Review === ${review}`)

    res.render('index.ejs', {
      pageTitle: 'Wine-o-Matic :: Um gerador de opinião',
      path: '/',
      review: review,
      editing: false
    })
  }
  