var rn = require('random-number');

var gen = rn.generator({
    min:  0
  , max:  6
  , integer: true
  })

var nota = rn.generator({
    min:  5
  , max:  10
  , integer: true
  })

let generatorService = {

    getReview: async function getReview() {

    let tanino = ['presentes','normais','fracos','curtos','jovens','marcantes','inexpressivos']
    let corpo = ['médio','fraco','massudo','redondo','leve','simpático','destacado']
    let retrogosto = ['envolvente', 'perspicaz','fraco','longo','curto','presente','refinado']
    let cor = ['leve','rubi escura','clara','jovial','quase transparente','amarelo ouro','palha seca']
    let presenca = ['morango', 'fumaça','bacon','blueberry','terra molhada','frutas do bosque','limao siciliano']
    let presenca2 = ['café', 'gordura','frutas cítricas','baunilha','chocolate amargo','abacaxi maduro','melao',]
    let alcool = ['resaltado','bem leve','bastante presente','firme e forte','quase imperceptível','além da conta','exagerado']

    let reviewCompleto = `Um vinho de corpo ${corpo[gen()]}, taninos ${tanino[gen()]}, com um retrogosto ${retrogosto[gen()]} e cor ${cor[gen()]}. Sentimos a presença de ${presenca[gen()]} e ${presenca2[gen()]}, salientando ainda o alcool ${alcool[gen()]}. Nota ${nota()}`

    return reviewCompleto

    }
}

module.exports = generatorService